/**
 * 
 */
package com.danyfel80.wellplates;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import org.xml.sax.SAXException;

import com.danyfel80.image.tileprovider.WellTileProvider;
import com.danyfel80.image.tilewriter.TiledTiffWriter;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.plugin.PluginDescriptor;
import icy.plugin.PluginLoader;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.dimension.Dimension2D;
import loci.formats.meta.IMetadata;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.hcs.data.Field;
import plugins.adufour.hcs.data.Well;
import plugins.adufour.hcs.data.WellPlate;
import plugins.adufour.hcs.io.WellPlateReader;
import plugins.adufour.hcs.io.WellPlateReader_Opera;

public class WellPlateStitching implements Callable<Void> {

	@FunctionalInterface
	public interface ProgressListener {
		void notifyProgress(String message, double progress);
	}

	private static final Set<WellPlateReader> availableReaders = new HashSet<WellPlateReader>();

	File inFolder;
	String filter;
	File outFolder;
	Dimension2D.Double gap;
	ProgressListener progressListener;

	public WellPlateStitching(File inFolder, String filter, File outFolder, Dimension2D.Double gap,
			ProgressListener progressListener) {
		this.inFolder = inFolder;
		this.filter = filter;
		this.outFolder = outFolder;
		this.gap = gap;
		this.progressListener = progressListener;
	}

	@SuppressWarnings("unchecked")
	private static void loadImporters() {
		availableReaders.clear();

		ArrayList<Class<WellPlateReader>> importers = new ArrayList<>();
		for (PluginDescriptor pd : PluginLoader.getPlugins(WellPlateReader.class)) {
			importers.add((Class<WellPlateReader>) pd.getPluginClass());
		}
		if (importers.isEmpty()) {
			importers.add((Class<WellPlateReader>) WellPlateReader_Opera.class.asSubclass(WellPlateReader.class));
		}

		// Add available file filters
		for (Class<WellPlateReader> importerClass : importers)
			try {
				availableReaders.add(importerClass.newInstance());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
	}

	private WellPlateReader getReaderFor(File file) {
		for (WellPlateReader reader : availableReaders)
			if (reader.isValidPlate(file))
				return reader;
		return null;
	}

	private WellPlateReader setupReader(File inFolder) throws RuntimeException {
		loadImporters();
		WellPlateReader reader = getReaderFor(inFolder);
		if (reader == null)
			throw new RuntimeException("No plate reader found for folder " + inFolder.getPath());
		return reader;
	}

	private WellPlate loadWellPlateData(WellPlateReader reader) throws RuntimeException {
		CancelableProgressFrame loadingProgress = new CancelableProgressFrame("Loading plate " + inFolder.getName());
		try {
			return reader.loadPlateFromFolder(inFolder, Optional.of(loadingProgress));
		} catch (Exception e) {
			throw new RuntimeException("Could not load plate", e);
		} finally {
			loadingProgress.close();
		}
	}

	private Dimension2D getPixelSize(WellPlate wellPlate, Well well) throws RuntimeException {
		try {
			Field aField = well.getFields().iterator().next();
			Sequence aSequence = wellPlate.loadField(aField);
			return new Dimension2D.Double(aSequence.getPixelSize());
		} catch (IOException | SAXException e) {
			throw new RuntimeException(e);
		}
	}

	private DataType getDataType(WellPlate wellPlate, Well well) throws RuntimeException {
		try {
			Field aField = well.getFields().iterator().next();
			Sequence aSequence = wellPlate.loadField(aField);
			return aSequence.getDataType_();
		} catch (IOException | SAXException e) {
			throw new RuntimeException(e);
		}
	}

	private Rectangle2D getWellRectangle(Well well) {
		Rectangle2D rect = null;
		for (Field field : well.getFields()) {
			Rectangle2D fieldBounds = field.getBounds();
			rect = ((rect == null) ? fieldBounds : rect.createUnion(fieldBounds));
		}
		return rect;
	}

	private Rectangle getWellRectangleInPixels(Rectangle2D rect, Dimension2D pixelSize) {
		return new Rectangle((int) (rect.getX() / pixelSize.getWidth()), (int) (rect.getY() / pixelSize.getHeight()),
				(int) ((rect.getWidth() + pixelSize.getWidth() - 1d) / pixelSize.getWidth()),
				(int) ((rect.getHeight() + pixelSize.getHeight() - 1d) / pixelSize.getHeight()));
	}

	private void notifyProgress(String message, double progress) {
		if (progressListener != null)
			progressListener.notifyProgress(message, progress);
	}

	@Override
	public Void call() throws InterruptedException, ClosedByInterruptException, RuntimeException {
		notifyProgress("Setting up reader...", 0.01d);
		WellPlateReader reader = setupReader(inFolder);
		notifyProgress("Loading well plate information...", 0.01d);
		if (Thread.currentThread().isInterrupted())
			throw new InterruptedException();
		WellPlate wellPlate = loadWellPlateData(reader);

		notifyProgress("Starting well processing...", 0.01d);

		// Find out how many wells are going to be processed...
		int wellTotal = 0;
		for (Iterator<Well> wellIterator = wellPlate.wellIterator(filter); wellIterator.hasNext();) {
			wellIterator.next();
			wellTotal++;
		}
		final int wellTotalF = wellTotal;

		// Process each well
		int wellPos = 0;
		for (Iterator<Well> wellIterator = wellPlate.wellIterator(filter); wellIterator.hasNext();) {
			if (Thread.currentThread().isInterrupted())
				throw new InterruptedException();
			Well well = wellIterator.next();
			final int wellPosF = wellPos;
			notifyProgress(String.format("Processing well %s (%d/%d)...", well.getAlphanumericID(), wellPos, wellTotal),
					Math.max(0.01d, wellPos / (double) wellTotal));

			// Retrieving size data
			Dimension2D pixelSize = getPixelSize(wellPlate, well);
			DataType dataType = getDataType(wellPlate, well);
			int channels = wellPlate.getMetaData().getChannelCount(0);
			Rectangle2D rect = getWellRectangle(well);
			Rectangle rectPx = getWellRectangleInPixels(rect, pixelSize);

			File outputFile = outFolder.toPath().resolve(well.getAlphanumericID() + ".ome.tif").toFile();

			// Create output image for current well
			try (WellTileProvider tileProvider = new WellTileProvider(wellPlate, well, pixelSize, channels, dataType, gap)) {
				TiledTiffWriter writer = new TiledTiffWriter(outputFile);
				IMetadata metadata = TiledTiffWriter.createMetadata(rectPx.width, rectPx.height, channels, dataType);

				OMEXMLMetadata additionalMetadata = null;
				try {
					additionalMetadata = wellPlate.loadField(well.fieldIterator().next()).getOMEXMLMetadata();
					// put additional metadata
					metadata.setImageName(well.getAlphanumericID(), 0);
					for (int i = 0; i < metadata.getChannelCount(0); i++) {
						metadata.setChannelName(additionalMetadata.getChannelName(0, i), 0, i);
						metadata.setChannelColor(additionalMetadata.getChannelColor(0, i), 0, i);
						metadata.setChannelEmissionWavelength(additionalMetadata.getChannelEmissionWavelength(0, i), 0, i);
						metadata.setChannelExcitationWavelength(additionalMetadata.getChannelExcitationWavelength(0, i), 0, i);
					}
					metadata.setPixelsPhysicalSizeX(additionalMetadata.getPixelsPhysicalSizeX(0), 0);
					metadata.setPixelsPhysicalSizeY(additionalMetadata.getPixelsPhysicalSizeY(0), 0);
					metadata.setPixelsPhysicalSizeZ(additionalMetadata.getPixelsPhysicalSizeZ(0), 0);
					metadata.setPixelsTimeIncrement(additionalMetadata.getPixelsTimeIncrement(0), 0);
					metadata.setInstrumentID(additionalMetadata.getInstrumentID(0), 0);
					metadata.setPlateID(additionalMetadata.getPlateID(0), 0);
					metadata.setPlateName(additionalMetadata.getPlateName(0), 0);
					metadata.setPlateColumns(additionalMetadata.getPlateColumns(0), 0);
					metadata.setPlateRows(additionalMetadata.getPlateRows(0), 0);
				} catch (IOException | SAXException e1) {
					System.err.println("No additional metadata");
				}

				writer.write(metadata, tileProvider, (String message, double progress) -> {
					notifyProgress(String.format("Processing well %s (%d/%d) %s...", well.getAlphanumericID(), wellPosF,
							wellTotalF, message), (wellPosF + progress) / (double) wellTotalF);
				});

			} catch (InterruptedException | ClosedByInterruptException e) {
				throw e;
			} catch (Exception e) {
				throw new RuntimeException("Could not write well", e);
			}
			wellPos++;
		}
		return null;
	}

}
