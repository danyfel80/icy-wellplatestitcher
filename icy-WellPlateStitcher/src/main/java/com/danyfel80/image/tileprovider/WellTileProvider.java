package com.danyfel80.image.tileprovider;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.EternalExpiryPolicy;
import javax.cache.spi.CachingProvider;

import org.xml.sax.SAXException;

import com.danyfel80.image.TileProvider;

import icy.image.IcyBufferedImage;
import icy.plugin.PluginLoader;
import icy.type.DataType;
import icy.type.dimension.Dimension2D;
import plugins.adufour.hcs.data.Field;
import plugins.adufour.hcs.data.Well;
import plugins.adufour.hcs.data.WellPlate;

/**
 * @author Daniel Felipe Gonzalez Obando
 *
 */
public class WellTileProvider implements TileProvider {

	public static int TILE_SIZE_X = 256;
	public static int TILE_SIZE_Y = 256;
	private WellPlate plate;
	private Well well;
	private Dimension2D pixelSize;
	private int sizeC;
	private DataType dataType;

	private Rectangle2D imageBounds;
	private Rectangle imageBoundsPx;
	private HashMap<Point, List<Field>> tileFields;
	Map<Double, Integer> fieldXs;
	Map<Double, Integer> fieldYs;

	private CacheManager cacheManager;
	private Cache<Integer, IcyBufferedImage> cache;
	private Dimension2D gap;

	public WellTileProvider(WellPlate plate, Well well, Dimension2D pixelSize, int sizeC, DataType dataType,
			Dimension2D gap) {
		this.plate = plate;
		this.well = well;
		this.pixelSize = pixelSize;
		this.sizeC = sizeC;
		this.dataType = dataType;
		this.gap = gap;

		// Find out the full image bounds in micro meters.
		this.imageBounds = null;
		{
			Set<Double> wellCols = new HashSet<>();
			Set<Double> wellRows = new HashSet<>();

			for (Iterator<Field> fieldIterator = this.well.fieldIterator(); fieldIterator.hasNext();) {
				Field field = fieldIterator.next();
				Rectangle2D fieldBounds = field.getBounds();
				wellCols.add(fieldBounds.getX());
				wellRows.add(fieldBounds.getY());
				imageBounds = ((imageBounds == null) ? fieldBounds : imageBounds.createUnion(fieldBounds));
			}
			List<Double> orderedFieldXs = wellCols.stream().sorted().collect(Collectors.toList());
			List<Double> orderedFieldYs = wellRows.stream().sorted().collect(Collectors.toList());

			this.fieldXs = new HashMap<>();
			this.fieldYs = new HashMap<>();

			int pos = 0;
			for (Double x : orderedFieldXs)
				fieldXs.put(x, pos++);
			pos = 0;
			for (Double x : orderedFieldYs)
				fieldYs.put(x, pos++);
		}

		// Rounded up image bounds
		this.imageBoundsPx = new Rectangle((int) (imageBounds.getX() / pixelSize.getWidth()),
				(int) (imageBounds.getY() / pixelSize.getHeight()),
				(int) ((imageBounds.getWidth() + pixelSize.getWidth() - 1d) / pixelSize.getWidth()),
				(int) ((imageBounds.getHeight() + pixelSize.getHeight() - 1d) / pixelSize.getHeight()));

		// Find out which fields are shown on each tile
		this.tileFields = new HashMap<Point, List<Field>>();
		for (Iterator<Field> fieldIterator = this.well.fieldIterator(); fieldIterator.hasNext();) {
			Field field = fieldIterator.next();
			Rectangle2D fieldBounds = field.getBounds();
			int fieldPositionX = fieldXs.get(fieldBounds.getX());
			int fieldPositionY = fieldYs.get(fieldBounds.getY());

			if (gap == null)
				gap = new Dimension2D.Double(0, 0);

			fieldBounds = new Rectangle2D.Double(field.getBounds().getX() - fieldPositionX * this.gap.getWidth(),
					field.getBounds().getY() - fieldPositionY * this.gap.getHeight(), field.getBounds().getWidth(),
					field.getBounds().getHeight());

			// Convert to pixels
			Rectangle2D fieldImageBounds = new Rectangle2D.Double(
					(fieldBounds.getX() - imageBounds.getMinX()) / pixelSize.getWidth(),
					(fieldBounds.getY() - imageBounds.getMinY()) / pixelSize.getHeight(),
					fieldBounds.getWidth() / pixelSize.getWidth(), fieldBounds.getHeight() / pixelSize.getHeight());
			int minTileX = (int) (fieldImageBounds.getMinX() / TILE_SIZE_X);
			int minTileY = (int) (fieldImageBounds.getMinY() / TILE_SIZE_Y);
			// using ceil function for max
			int maxTileX = (int) ((fieldImageBounds.getMaxX() + TILE_SIZE_X - 1d) / TILE_SIZE_X);
			int maxTileY = (int) ((fieldImageBounds.getMaxY() + TILE_SIZE_Y - 1d) / TILE_SIZE_Y);

			for (int y = minTileY; y < maxTileY; y++) {
				for (int x = minTileX; x < maxTileX; x++) {
					Point tile = new Point(x, y);
					List<Field> fields = tileFields.get(tile);
					if (fields == null) {
						fields = new ArrayList<>(4);
						tileFields.put(tile, fields);
					}
					fields.add(field);
				}
			}
		}

		// Create image cache
		CachingProvider provider = Caching.getCachingProvider("org.ehcache.jsr107.EhcacheCachingProvider", PluginLoader.getLoader());
		cacheManager = provider.getCacheManager();
		MutableConfiguration<Integer, IcyBufferedImage> configuration = new MutableConfiguration<Integer, IcyBufferedImage>()
				.setTypes(Integer.class, IcyBufferedImage.class).setStoreByValue(false)
				.setExpiryPolicyFactory(EternalExpiryPolicy.factoryOf());
		cache = cacheManager.createCache("imageCache", configuration);
	}

	@Override
	public IcyBufferedImage getTile(Point tile) throws IOException {
		int x = tile.x * TILE_SIZE_X;
		int y = tile.y * TILE_SIZE_Y;
		int w = (x + TILE_SIZE_X < imageBoundsPx.width ? TILE_SIZE_X : imageBoundsPx.width - x);
		int h = (y + TILE_SIZE_Y < imageBoundsPx.height ? TILE_SIZE_Y : imageBoundsPx.height - y);
		Rectangle tileBounds = new Rectangle(x, y, w, h);
		List<Field> fields = tileFields.getOrDefault(tile, new ArrayList<>());
		IcyBufferedImage image = new IcyBufferedImage(w, h, sizeC, dataType);

		image.beginUpdate();
		try {
			for (Field field : fields) {
				Rectangle fieldBoundsPx = convertToImagePixels(field.getBounds());

				IcyBufferedImage fieldImage = cache.get(field.getID());
				if (fieldImage == null) {
					fieldImage = plate.loadField(field).getFirstImage();
					cache.put(field.getID(), fieldImage);
				}

				Rectangle fieldPortion = fieldBoundsPx.intersection(tileBounds);
				fieldPortion = new Rectangle(fieldPortion.x - fieldBoundsPx.x, fieldPortion.y - fieldBoundsPx.y,
						fieldPortion.width, fieldPortion.height);

				image.copyData(fieldImage, fieldPortion, new Point(fieldBoundsPx.x - tileBounds.x + fieldPortion.x,
						fieldBoundsPx.y - tileBounds.y + fieldPortion.y));
			}
		} catch (SAXException | IOException e) {
			throw new IOException(e);
		} finally {
			image.endUpdate();
		}
		return image;
	}

	private Rectangle convertToImagePixels(Rectangle2D bounds) {
		int fieldPositionX = fieldXs.get(bounds.getX());
		int fieldPositionY = fieldYs.get(bounds.getY());

		return new Rectangle(
				(int) (((bounds.getX() - imageBounds.getMinX() - fieldPositionX * gap.getWidth()) / pixelSize.getWidth())),
				(int) (((bounds.getY() - imageBounds.getMinY() - fieldPositionY * gap.getHeight()) / pixelSize.getHeight())),
				(int) ((bounds.getWidth() / pixelSize.getWidth()) + 0.5d),
				(int) ((bounds.getHeight() / pixelSize.getHeight()) + 0.5d));
	}

	@Override
	public void close() throws Exception {
		cacheManager.close();
	}
}
