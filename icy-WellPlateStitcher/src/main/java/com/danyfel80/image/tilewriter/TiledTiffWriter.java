package com.danyfel80.image.tilewriter;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.danyfel80.image.TileProvider;
import com.danyfel80.wellplates.WellPlateStitching.ProgressListener;

import icy.image.IcyBufferedImage;
import icy.sequence.MetaDataUtil;
import icy.type.DataType;
import icy.util.OMEUtil;
import loci.common.services.DependencyException;
import loci.common.services.ServiceException;
import loci.formats.FormatException;
import loci.formats.meta.IMetadata;
import loci.formats.ome.OMEXMLMetadata;
import loci.formats.out.OMETiffWriter;
import loci.formats.tiff.IFD;
import loci.formats.tiff.TiffCompression;

public class TiledTiffWriter {

	private File			fileName;
	public static int	tileWidth		= 256;
	public static int	tileHeight	= 256;

	public TiledTiffWriter(File fileName) {
		this.fileName = fileName;
	}

	public void write(IMetadata metadata, TileProvider tileProvider, ProgressListener progressListener)
			throws FormatException, IOException, InterruptedException {
		OMETiffWriter writer = initializeWriter(metadata);
		try {
			int x, y;
			byte[] tile;
			long[] rowsPerStrip;
			int w, h;
			IFD ifd;
			int count;
			int sizeX, sizeY;
			int n, m;
			int diffWidth, diffHeight;

			rowsPerStrip = new long[1];
			rowsPerStrip[0] = tileHeight;

			int series = 1;
			for (int s = 0; s < series; s++) {
				sizeX = metadata.getPixelsSizeX(0).getValue();
				sizeY = metadata.getPixelsSizeY(0).getValue();
				if (tileWidth <= 0) tileWidth = sizeX;
				if (tileHeight <= 0) tileHeight = sizeY;
				n = sizeX / tileWidth;
				m = sizeY / tileHeight;
				if (n == 0) {
					tileWidth = sizeX;
					n = 1;
				}
				if (m == 0) {
					tileHeight = sizeY;
					m = 1;
				}
				diffWidth = sizeX - n * tileWidth;
				diffHeight = sizeY - m * tileHeight;
				if (diffWidth > 0) n++;
				if (diffHeight > 0) m++;

				count = metadata.getPixelsSizeC(0).getValue();
				int totalTiles = m * n * count;
				int tilesProcessed = 0;
				for (int k = 0; k < count; k++) {
					x = 0;
					y = 0;
					ifd = new IFD();
					ifd.put(IFD.TILE_WIDTH, tileWidth);
					ifd.put(IFD.TILE_LENGTH, tileHeight);
					ifd.put(IFD.ROWS_PER_STRIP, rowsPerStrip);
					for (int i = 0; i < m; i++) {
						y = tileHeight * i;
						if (diffHeight > 0 && i == (m - 1)) {
							h = diffHeight;
						} else {
							h = tileHeight;
						}
						for (int j = 0; j < n; j++) {
							if (Thread.currentThread().isInterrupted()) throw new InterruptedException();
							x = tileWidth * j;
							if (diffWidth > 0 && j == (n - 1)) {
								w = diffWidth;
							} else {
								w = tileWidth;
							}

							notifyProgress(progressListener, String.format("Tile (%d/%d)", tilesProcessed, totalTiles),
									tilesProcessed / (double) totalTiles);

							IcyBufferedImage tileImage = tileProvider.getTile(new Point(j, i));
							// Icy.getMainInterface().addSequence(new
							// Sequence(IcyBufferedImageUtil.getCopy(tileImage)));
							tile = tileImage.getRawData(k, !metadata.getPixelsBinDataBigEndian(0, 0));
							if (tileImage.getWidth() != w || tileImage.getHeight() != h)
								throw new IOException(String.format("Tile size not coherent: Tile (%d, %d), expected (%d, %d)",
										tileImage.getWidth(), tileImage.getHeight(), w, h));
							writer.saveBytes(k, tile, ifd, x, y, w, h);
							tilesProcessed++;
						}
					}
				}
			}
		} finally {
			writer.close();
		}
	}

	private void notifyProgress(ProgressListener progressListener, String message, double progress) {
		if (progressListener != null) {
			progressListener.notifyProgress(message, progress);
		}
	}

	private OMETiffWriter initializeWriter(IMetadata metadata) throws FormatException, IOException {
		OMETiffWriter writer = new OMETiffWriter();
		writer.setCompression(TiffCompression.LZW.getCodecName());
		writer.setMetadataRetrieve(metadata);
		writer.setWriteSequentially(true);
		writer.setInterleaved(false);
		writer.setBigTiff(true);

		Files.deleteIfExists(fileName.toPath());
		writer.setId(fileName.getAbsolutePath());
		writer.setSeries(0);
		return writer;
	}

	public static IMetadata createMetadata(int width, int height, int channels, DataType dataType)
			throws DependencyException, ServiceException {
		OMEXMLMetadata metadata = (OMEXMLMetadata) OMEUtil.createOMEXMLMetadata();
		MetaDataUtil.setMetaData(metadata, width, height, channels, 1, 1, dataType, true);
		return metadata;
	}
}
