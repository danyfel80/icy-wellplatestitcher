package plugins.danyfel80.wellplates;

import java.io.IOException;
import java.nio.file.Paths;

import com.danyfel80.image.tileprovider.ImageTileProvider;
import com.danyfel80.image.tilewriter.TiledTiffWriter;

import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.image.IcyBufferedImage;
import loci.common.services.DependencyException;
import loci.common.services.ServiceException;
import loci.formats.FormatException;
import loci.formats.meta.IMetadata;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarFolder;
import plugins.adufour.ezplug.EzVarSequence;

/**
 * @author daniel
 */
public class SequenceTileWriterTest extends EzPlug {

	EzVarSequence	inputSequence;
	EzVarFolder		outputFolder;

	@Override
	protected void initialize() {
		inputSequence = new EzVarSequence("Sequence");
		outputFolder = new EzVarFolder("Folder", null);
		addEzComponent(inputSequence);
		addEzComponent(outputFolder);
	}

	@Override
	protected void execute() {
		TiledTiffWriter writer = new TiledTiffWriter(
				Paths.get(outputFolder.getValue(true).getAbsolutePath(), "image.ome.tiff").toFile());

		IcyBufferedImage image = inputSequence.getValue(true).getFirstImage();
		try {
			IMetadata meta = TiledTiffWriter.createMetadata(image.getWidth(), image.getHeight(), image.getSizeC(),
					image.getDataType_());
			writer.write(meta, new ImageTileProvider(image), (String message, double progress) -> {
				getUI().setProgressBarMessage(message);
				getUI().setProgressBarValue(progress);
			});
		} catch (IOException | DependencyException | ServiceException | FormatException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			new FailedAnnounceFrame("Sequence tile write test was interrupted", 10);
		}
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}
}
