package plugins.danyfel80.wellplates;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.danyfel80.wellplates.WellPlateStitching;

import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.system.IcyHandledException;
import icy.type.dimension.Dimension2D;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarDoubleArrayNative;
import plugins.adufour.ezplug.EzVarFolder;
import plugins.adufour.ezplug.EzVarText;

/**
 * @author daniel
 */
public class WellPlateStitcher extends EzPlug implements EzStoppable {

	private EzVarFolder varInInputFolder;
	private EzVarText varInFilter;
	private EzVarFolder varInOutputFolder;
	private EzVarDoubleArrayNative varInGap;

	private ExecutorService execution;

	@Override
	protected void initialize() {
		varInInputFolder = new EzVarFolder("Plate folder", null);
		varInInputFolder.setToolTipText("Folder containing plate files.");
		varInFilter = new EzVarText("Filter wells", "");
		varInFilter.setToolTipText(
				"One or more templates (separated by spaces) used to select only a subset of wells, e.g.: \"A 03 B12\"");
		varInOutputFolder = new EzVarFolder("Output folder", null);
		varInOutputFolder.setToolTipText("Folder containing resulting images.");
		varInGap = new EzVarDoubleArrayNative("Gap in X and Y", new double[][] { new double[] { 0d, 0d } }, true);
		varInGap.setToolTipText("Space to move each field in X and Y axes in microns");

		addEzComponent(varInInputFolder);
		addEzComponent(varInFilter);
		addEzComponent(varInOutputFolder);
		addEzComponent(varInGap);
	}

	@Override
	protected void execute() {
		getUI().setProgressBarVisible(true);

		File inFolder = varInInputFolder.getValue(true);
		String filter = varInFilter.getValue(true);
		File outFolder = varInOutputFolder.getValue(true);
		double[] gapVals = varInGap.getValue(true);
		if (gapVals.length != 2)
			throw new IcyHandledException("Gap parameter must specify space for x and y");

		execution = Executors.newSingleThreadExecutor();

		WellPlateStitching task = new WellPlateStitching(inFolder, filter, outFolder, new Dimension2D.Double(gapVals), (String message, double progress) -> {
			if (getUI() != null) {
				getUI().setProgressBarMessage(message);
				getUI().setProgressBarValue(progress);
			}
		});
		
		Future<Void> future = execution.submit(task);
		execution.shutdown();
		try {
			future.get();
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			new FailedAnnounceFrame("Well plates was interrupted", 10);
		}
	}

	@Override
	public void stopExecution() {
		if (execution != null && !execution.isTerminated()) {
			execution.shutdownNow();
			try {
				if (!execution.awaitTermination(5, TimeUnit.SECONDS)) {
					throw new RuntimeException("Tried to stop execution but it's till running");
				}
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		super.stopExecution();
	}

	@Override
	public void clean() {
	}

}
